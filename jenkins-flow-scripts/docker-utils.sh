#!/bin/bash

verify_docker_login(){

USERNAME=$1
PASSWORD=$2

 if ! docker info 2>&1 | grep -q 'ERROR: Cannot connect to the Docker daemon'; then
        echo "Docker is running, checking if logged in..."

        # Check if already logged in by looking for a username in the docker info output
        if docker info 2>&1 | grep -q 'Username'; then
            echo "Already logged into Docker."
        else
            echo "Not logged into Docker, attempting to log in..."
            # Log in to Docker using the provided username and password
            echo $PASSWORD | docker login --username $USERNAME --password-stdin
            if [ $? -eq 0 ]; then
                echo "Docker login successful."
                return 0
            else
                echo "Docker login failed."
                return 1
            fi
        fi
    else
        echo "Docker is not running or you do not have permission to access it."
        return 1
    fi

}

verify_image_exist(){
    IMAGENAME=$1 
    # Check if the image is available in docker 
    availableImage=$(docker images | grep -w "$IMAGENAME" | wc -l)
    if [ "$availableImage" -gt 0 ]; then
         echo "Image $IMAGENAME is available locally."
         return 0
    else
         echo "Image $IMAGENAME is not available locally."
         return 1
    fi
}

verify_container_up(){
    CONTAINERNAME=$1 
    activeContainers=$(docker ps | grep -w "$CONTAINERNAME" | wc -l)
    if [ "$activeContainers" -eq 0 ]; then 
        echo "container $CONTAINERNAME is not active"
        return 1 
    else 
        echo "container $CONTAINERNAME is active "
        return 0
    fi

}

verify_container_available(){
    CONTAINERNAME=$1 
    availableContainers=$(docker ps -a | grep -w "$CONTAINERNAME" | wc -l)
    if [ "$availableContainers" -eq 0 ] ; then 
        echo "container $CONTAINERNAME was never used or was deleted"
        return 1 
    else 
        echo "container $CONTAINERNAME is available and not up"
        return 0
    fi
}


# "idubi/curl-alpine-operator:latest" \
# "curl-execution"
# "docker run -d --name curl-execution --privileged -v /home/jenkins-agent/curl:/app  -v /home/jenkins-agent/logs:/logs -v /home/jenkins-agent/artifacts:/artifacts idubi/curl-alpine-operator:latest"
# $DOCKER_USERNAME \
# $DOCKER_PASSWORD 

execute_docker_container() {
    DOCKERIMAGE=$1
    DOCKERCONTAINER=$2 
    COMMAND=$3
    USER=$4
    PASS=$5
    
    echo "inside execute docker container $DOCKERCONTAINER $USER $PASS "
    if (verify_container_up $DOCKERCONTAINER) ; then
        echo "failed - the builder is executing, cant build on same active container" 
        return 1
    else
        if (verify_container_available $DOCKERCONTAINER) ; then
            echo "need to start $DOCKERCONTAINER"
            docker start $DOCKERCONTAINER
        else
            if (verify_image_exist $DOCKERIMAGE) ; then
                 echo "need to run image $DOCKERIMAGE"
                 $COMMAND
            else
                if (verify_docker_login $USER $PASS) ; then
                   echo "need to run image $DOCKERIMAGE  this will download from dockerhub"
                   $COMMAND
                   return 0
                else 
                   echo "failed to load container $DOCKERCONTAINER and image $DOCKERIMAGE " 
                   return 1
                fi
            fi
        fi
    fi
    return 0
}


cleanup_old_local_repo() {
    DOMAIN=$1
    REPONAME=$2
    

    containerExists=$(docker ps -a --format "{{.Image}}" | grep "$REPONAME")
    if [ ! -z "$containerExists" ]; then
        docker rm -f $REPONAME
        docker rmi -f $(docker images "$REPONAME" -q)
    fi

    # Check if the repository image exists
    repoExists=$(docker images | grep "$DOMAIN/$REPONAME")
    if [ ! -z "$repoExists" ]; then
        docker rmi -f $(docker images "$DOMAIN/$REPONAME" -q)
    fi
}


stop_container() {
    CONTAINERNAME=$1
    if docker ps | grep -q "$CONTAINERNAME"; then
        echo `docker stop $CONTAINERNAME`;
    fi
} 

wait_while_container_up() {
    CONTAINERNAME=$1
    while docker ps | grep -q "$CONTAINERNAME"; do
        echo "Waiting for container $CONTAINERNAME to stop..."
        sleep 10
    done
}

is_container_up() {
    CONTAINERNAME=$1
    if docker ps | grep -q "$CONTAINERNAME"; then
        return 0
    else
        return 1
    fi
}
