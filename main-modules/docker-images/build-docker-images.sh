
# build docker agent 
# build the docker image to be created for runing the jenkins agent
# ALPINE DOCKER

# # dk run -d --name=jenkins-agent --restart unless-stopped --privileged -v /var/run/docker.sock:/var/run/docker.sock idubi/alpine-jenkins-agent:latest

dk build -t idubi/alpine-dind-jenkins-agent:latest -f jenkins-agent-image/Dockerfile.dind.jenkins-agent ./jenkins-agent-image
dk run -d --name=jenkins-dind-agent --restart unless-stopped --privileged idubi/alpine-dind-jenkins-agent:latest
dk logs jenkins-dind-agent
dk exec -it jenkins-dind-agent /bin/sh

dk rm -f jenkins-dind-agent && dk rmi idubi/alpine-dind-jenkins-agent:latest



dk build -t idubi/curl-alpine-operator:latest -f curl-image/Dockerfile.alpine ./curl-image
docker run -d --name curl-execution --privileged -v /home/jenkins-agent/curl:/app  -v /home/jenkins-agent/logs:/logs -v /home/jenkins-agent/artifacts:/artifacts idubi/curl-alpine-operator:latest
dk logs curl-execution
dk exec -it curl-execution /bin/sh

dk rm -f curl-execution && dk rmi idubi/curl-alpine-operator:latest

docker rm -f curl-execution  && docker rmi idubi/curl-alpine-operator:latest

# docker rm -f curl-execution && docker rmi curl-ubuntu-operator

