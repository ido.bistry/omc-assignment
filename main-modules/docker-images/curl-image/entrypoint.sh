#!/bin/bash
# Set the working directory to the mounted volume
WORK_DIR="/app"
cd "$WORK_DIR"

# perpare python env
python -m venv venv 
. ./venv/bin/activate

# Execute the commands
autoreconf -fi > /logs/prepare.log 
./configure --with-openssl --disable-shared --enable-debug --enable-maintainer-mode > /logs/config.log 


make > /logs/make.log 2>&1


make test > /logs/make_test.log 2>&1
if [ $? -eq 0 ]; then
    make install > /logs/install.log 2>&1
    cp /usr/local/bin/curl /artifacts
fi



#Keep the container running
#tail -f /dev/null

# this make sure the entrypoint will execute every start 
exec "$@"