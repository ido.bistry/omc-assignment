#!/bin/bash
if [ ! -f /home/jenkins-agent/.firstrun ]; then
    echo "This is the first run ( ==>  ${PWD}  ) "
    curl -sO http://omc-jenkins:8080/jnlpJars/agent.jar
    touch /home/jenkins-agent/.firstrun
fi
echo "This is not the first run - the container is restarted"
if [ -f /home/jenkins-agent/secret-file ]; then
   nohup java -jar agent.jar -url http://omc-jenkins:8080/ -secret @secret-file -name "jenkins-agent" -workDir "/home/jenkins-agent" -webSocket > agent_$(date "+%d%m%Y-%H%M").log 2>&1 &
fi
# Execute the main container command
exec /usr/local/bin/dockerd-entrypoint.sh "$@"