#!/bin/bash
if [ ! -f /var/.firstrun ]; then
    echo "This is the first run"
    # sudo chown root:docker /var/run/docker.sock
    sudo chmod  777 /var/run/docker.sock
    # sudo mkdir /home/jenkins-agent && sudo chown -R $(whoami):$(whoami) /home/jenkins-agent
    curl -sO http://omc-jenkins:8080/jnlpJars/agent.jar
    # Create the .firstrun file so this block won't run again
    sudo touch /var/.firstrun
fi
if [ -f /home/jenkins-agent/secret-file ]; then
   nohup java -jar agent.jar -url http://omc-jenkins:8080/ -secret @secret-file -name "jenkins-agent" -workDir "/home/jenkins-agent" -webSocket > agent_$(date "+%d%m%Y-%H%M").log 2>&1 &
fi
# Execute the main container command
exec "$@"