#!/bin/bash

prepare_clean_env() {
    if [ ! -f /var/.firstrun ]; then
        rm -rf {$HOME}/logs/* 
        rm -rf {$HOME}/artifacts/* 
        sudo rm -rf {$HOME}/curl
        mkdir {$HOME}/curl 
    fi
}


prepare_data(){
    rsync ${HOME}/workspace/curl-clone {$HOME}/curl -a
}