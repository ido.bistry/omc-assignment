#!/bin/bash


# set minikube loadbalancer
minikube tunnel

#start minikube dashboard
minikube dashboard

# create namespace and set working namespace
kubectl create ns jenkins
kubectl config set-context --current --namespace=jenkins
kubectl config view | grep namespace

# install helm
helm repo add jenkins https://charts.jenkins.io
helm repo update  


#install jenkins volume persistent 
kubectl apply -f ./jenkins-volums-persistent.yaml





ssh docker@$(minikube ip) -i $(minikube ssh-key) 'sudo mkdir -p /tmp/hostpath-provisioner/jenkins/omc-jenkins && sudo chmod 777 /tmp/hostpath-provisioner/jenkins/omc-jenkins'
ssh docker@$(minikube ip) -i $(minikube ssh-key) 'sudo chmod 777 /tmp/hostpath-provisioner/jenkins-agent'

scp -i $(minikube ssh-key) -r /home/ubuntu/projects/omc-assignment/main-modules/jenkins/sync-master-agent/*  docker@$(minikube ip):/home/docker/
ssh docker@$(minikube ip) -i $(minikube ssh-key) 'sudo chmod +x /home/docker/*'
ssh docker@$(minikube ip) -i $(minikube ssh-key) 'nohup /home/docker/sync-logs-between-master-2-slave.sh > /home/docker/sync-logs-between-master-2-slave.log  2>&1 &'

# kill the nohup for sync 
# ssh docker@$(minikube ip) -i $(minikube ssh-key) '/home/docker/kill-sync-processes.sh'

# dev --> copy from master  to here 
# scp -i $(minikube ssh-key) -r docker@$(minikube ip):/home/docker  /home/ubuntu/projects/omc-assignment/main-modules/jenkins/

#copy all data from local to pv --> pvc --> pod , whenl  loading 
scp -i $(minikube ssh-key) -r /home/ubuntu/projects/omc-assignment/main-modules/jenkins/data/jenkins-master/* docker@$(minikube ip):/tmp/hostpath-provisioner/jenkins/omc-jenkins
scp -i $(minikube ssh-key) -r /home/ubuntu/projects/omc-assignment/main-modules/jenkins/data/jenkins-agent/* docker@$(minikube ip):/tmp/hostpath-provisioner/jenkins-agent



#copy all data from pod to local
# scp -i $(minikube ssh-key) -r docker@$(minikube ip):/tmp/hostpath-provisioner/jenkins/omc-jenkins/* /home/ubuntu/projects/omc-assignment/main-modules/jenkins/data/jenkins-master
# scp -i $(minikube ssh-key) -r docker@$(minikube ip):/tmp/hostpath-provisioner/jenkins-agent/* /home/ubuntu/projects/omc-assignment/main-modules/jenkins/data/jenkins-agent
# scp -i $(minikube ssh-key) -r docker@$(minikube ip):/home/docker  /home/ubuntu/projects/omc-assignment/main-modules/jenkins/

# inside minikube rsync ~/jenkins-master/data / .


# k upply -f ./jenkins-secret-github.yaml

#install jekins master
helm upgrade -i omc-jenkins jenkins/jenkins --namespace=jenkins --values=./jenkins-helm-values.yaml


# pvc > host >>>> pod  jenkins/omc-jenkins 
#       (minikube ssh) ---->  docker host of minikube --->   /home/ubuntu/projects/OMC-assignmen   ....>>>>>...  

#       (kubectl exec --namespace jenkins  -it pod/omc-jenkins-0 sh) ---> jenkins home in pod  ---->  /home/jenkins

# #copy all data from pod to local
# scp -i $(minikube ssh-key) -r docker@$(minikube ip):/tmp/hostpath-provisioner/jenkins/omc-jenkins-bk /home/ubuntu/projects/OMC-assignment/jenkins/data
# #rsync data from local to pod 
# rsync -avz -e "ssh -i $(minikube ssh-key)" /tmp/hostpath-provisioner/jenkins/omc-jenkins-bk docker@$(minikube ip):/home/ubuntu/projects/OMC-assignment/jenkins/data/omc-jenkins/*

# from minikube user (minikube ssh) :  sudo chmod 777 /tmp/hostpath-provisioner/jenkins/omc-jenkins





# install jenkins agent
kubectl apply -f ./jenkins-agent-deployment.yaml



# rsync -avz -e "ssh -i $(minikube ssh-key)" /tmp/hostpath-provisioner/jenkins/omc-jenkins-bk docker@$(minikube ip):/home/ubuntu/projects/OMC-assignment/jenkins/data/omc-jenkins/*



#github secret for jenkins:  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


# build thedocker image to be created for runing the jenkins agent
# docker build -t alpine-jenkins-agent -f Dockerfile.jenkins-agent .
# dk run -d --name=jenkins-agent --restart unless-stopped --privileged -v /var/run/docker.sock:/var/run/docker.sock alpine-jenkins-agent



# sudo docker exec -it jenkins-agent sh
# dk stop jenkins-agent && dk rm jenkins-agent && dk rmi alpine-jenkins-agent
# dk rmi alpine-jenkins-agent

# kubectl apply -f ./jenkins-volums-persistent.yaml

# sudo docker stop jenkins-agent && sudo docker rm jenkins-agent
# docker stop jenkins-agent && docker rm jenkins-agent


# execute on pod for connection it to jenkins master 
# sudo chown -R $(whoami):$(whoami) /var/jenkins-agent
# curl -sO http://omc-jenkins:8080/jnlpJars/agent.jar
# java -jar agent.jar -url http://omc-jenkins:8080/ -secret 7ec4d5f1d4c5b38be3df06435231e4122ba7254d9ba6854f3e3c5fbf29421935 -name "jenkins-agent" -workDir "/var/jenkins-agent" -webSocket 
  

