####DOCKER - DEPLOY ELASTIC CLOUD ON KUBERNETES (ECK) VIA ELASTICSEARCH OPERATOR ON MINIKUBE####
https://www.bogotobogo.com/DevOps/Docker/Docker_Kubernetes_Elastic_Cloud_on_Kubernetes_ECK_minikube.php

#preperation : 




# install  istio mesh as load balancer mesh for connection with the outer world 
curl -L https://istio.io/downloadIstio | sh -



kubectl create namespace jenkins


now download jenkins and install it : 
   helm repo add jenkins https://charts.jenkins.io
   helm repo update  

   helm search repo <repo alias>

   # NAME            CHART VERSION   APP VERSION     DESCRIPTION                                       
   # jenkins/jenkins 5.1.6           2.440.3         Jenkins - Build great things at any scale! As t...   

   helm show values jenkins/jenkins
   helm pull jenkins/jenkins --untar


   helm install jenksin/jenkins --namespace=jenkins -f=./consig/jenkins-        

#kubectl --namespace jenkins port-forward svc/jenkins 8080:8080

kubectl label namespace jenkins istio-injection=enabled

helm upgrade -i jenkins jenkins/jenkins --namespace=jenkins --values=./config/jenkins-config-values_3.yaml  

kubectl --namespace jenkins port-forward svc/jenkins 8081:8081

