#!/bin/bash

# Source and destination directories
SOURCE_DIR="/tmp/hostpath-provisioner/jenkins/omc-jenkins/jobs/omc-assignment/builds"
DEST_DIR="/tmp/hostpath-provisioner/jenkins-agent/master-build-logs/omc-assignment"

# Function to sync log files
sync_logs() {
    local build_number="$1"
    echo "Syncing logs for build number $build_number"
    local source_log="$SOURCE_DIR/$build_number/log"
    local dest_log="$DEST_DIR/build_${build_number}.log"

    # Use rsync to sync the log file to the destination with the desired naming convention
    rsync -avz "$source_log" "$dest_log"
}

# Main loop to monitor for changes
echo "Monitoring for changes in $SOURCE_DIR"
while inotifywait -r -e modify,create "$SOURCE_DIR"; do
    # Find all build directories
    find "$SOURCE_DIR" -mindepth 1 -maxdepth 1 -type d | while read build_dir; do
        # Extract the build number from the directory name
        build_number=$(basename "$build_dir")

        # Sync the log for this build number
        sync_logs "$build_number"
    done
done