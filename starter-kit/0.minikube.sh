#!/bin/bash

# minikube tunnel creat ean inres service for 
# all of the minikube loadbalancers 
# I dont execute it as nohup because it is difficult 
# to control it once started and sometimes fail minikube 
# and need force restart 
# this will be in a seperate process 
sudo minikube tunnel
