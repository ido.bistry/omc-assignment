
#!/bin/bash

kubectl create ns jenkins
kubectl config set-context --current --namespace=jenkins
kubectl config view | grep namespace

helm repo add jenkins https://charts.jenkins.io
helm repo update 


#install jenkins volume persistent 
kubectl apply -f ../main-modules/jenkins/jenkins-volums-persistent.yaml


# setting the permissions for the jenkins pv
ssh docker@$(minikube ip) -i $(minikube ssh-key) 'sudo mkdir -p /tmp/hostpath-provisioner/jenkins/omc-jenkins && sudo chmod 777 /tmp/hostpath-provisioner/jenkins/omc-jenkins'
ssh docker@$(minikube ip) -i $(minikube ssh-key) 'sudo mkdir -p /tmp/hostpath-provisioner/jenkins-agent && sudo chmod 777 /tmp/hostpath-provisioner/jenkins-agent'


# adding the starter files to jenkins pv : the directory structure , and the jenkins plugins  configuration and work
#copy all data from local to pv --> pvc --> pod , whenl  loading 
scp -i $(minikube ssh-key) -r ../main-modules/jenkins/data/jenkins-master/* docker@$(minikube ip):/tmp/hostpath-provisioner/jenkins/omc-jenkins
scp -i $(minikube ssh-key) -r ../main-modules/jenkins/data/jenkins-agent/* docker@$(minikube ip):/tmp/hostpath-provisioner/jenkins-agent


# setting the permissions for the minkube home -where we executethe sync from (see next explenation)
ssh docker@$(minikube ip) -i $(minikube ssh-key) 'sudo chmod +x /home/docker/*'
ssh docker@$(minikube ip) -i $(minikube ssh-key) 'sudo apt update && sudo apt upgrade && sudo apt install inotify-tools'
scp -i $(minikube ssh-key) -r ../main-modules/jenkins/sync-master-agent/*  docker@$(minikube ip):/home/docker/

# execute the synchroniztion  between master jenkins and agent jenkins 
# to enable the sync between the two jenkins logs 
# while job is executed - it is syc to the agent , so later on we execute a job inside agent to read the logs and 
# send it to repo (I couldnt use the logstash ) 
ssh docker@$(minikube ip) -i $(minikube ssh-key) 'nohup /home/docker/sync-logs-between-master-2-slave.sh > /home/docker/sync-logs-between-master-2-slave.log  2>&1 &'
# if we want to kill the sync process , thisi s what we need to execute 
# ssh docker@$(minikube ip) -i $(minikube ssh-key) '/home/docker/kill-sync-processes.sh'


#install jekins comntorller 
## --->   I have issue with plugins , so I copied the plugins into the pods the old fashion way 
##    --> te ideal will be to load it form a file 
##    --> to overcome this issue I added the use of lfs in github , so the plugins will be loaded from the repo
##    --> thisi s a thing to hanle later. 
helm upgrade -i omc-jenkins jenkins/jenkins --namespace=jenkins --values=../main-modules/jenkins/jenkins-helm-values.yaml


 # wait until 2/2 are ready 
 watch -n 4  -c 'echo "wait until 2/2 are ready"  &&  kubectl get pods omc-jenkins-0 -n jenkins'


# install jenkins agent only afte the jenksin master is up and running
# or it might be an issue with the connectivity
kubectl apply -f ../main-modules/jenkins/jenkins-agent-deployment.yaml


# if we have issue with connectivity from agent 
# a delete and then ally will fix it 
#kubectl delete -f ../main-modules/jenkins/jenkins-agent-deployment.yaml

# wait until 3 are 1/1 are ready
watch -n 4  -c 'echo "wait until 3 are 1/1 are ready"  &&   kubectl get pods -n jenkins | grep "alpine-jenkins-agent"'

minikube dashboard

url : 
# sevice : omc-jenkins ==> link 
minikube service omc-jenkins --url --namespace=jenkins

#05:06:34 ubuntu@ubuntu-virtual-machine omc-assignment ±|main ✗|→ minikube service omc-jenkins --url --namespace=jenkins
#http://192.168.49.2:31866



