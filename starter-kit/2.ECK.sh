#!/bin/bash

 
# minikube config set memory 20G  
# minikube stop 
# minikube start 
#  set minikube tunnel again and dashboard 


kubectl create ns elastic-system
kubectl config set-context --current --namespace=elastic-system
kubectl config view | grep namespace

kubectl apply -f https://download.elastic.co/downloads/eck/2.12.1/crds.yaml  


kubectl apply -f https://download.elastic.co/downloads/eck/2.12.1/operator.yaml  


kubectl logs -f statefulset.apps/elastic-operator



kubectl apply -f ../main-modules/ECK/elastic-cluster.yaml 


# wait untill health = green 
# about 3 minutes
watch -n 4 -c 'echo "wait untill HEALTH == green"  &&    kubectl get elasticsearch'



kubectl get pods --selector='elasticsearch.k8s.elastic.co/cluster-name=omc-test'

kubectl logs -f omc-test-es-default-0

kubectl get service omc-test-es-http
# NAME               TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)    AGE
# omc-test-es-http   ClusterIP   10.102.53.71   <none>        9200/TCP   7m6s





PASSWORD=$(kubectl get secret omc-test-es-elastic-user -o go-template='{{.data.elastic | base64decode}}')

echo     "write this down : $PASSWORD " 
#870P1BbQ5x5401o9zVytSgsv

kubectl port-forward services/omc-test-es-http 9200
# request endpoint 
# curl -u "elastic:$PASSWORD" -k "https://127.0.0.1:9200"
# from vm :



kubectl apply -f ../main-modules/ECK/kibana-cluster.yaml 


watch -n 4 -c 'echo "wait untill HEALTH == green"  &&    kubectl get kibana'

 k apply -f ../main-modules/ECK/logstash-configmap.yaml
 k apply -f ../main-modules/ECK/logstash-cluster.yaml

 #look at service : omc-test-kb-http  
 kubectl get service omc-test-kb-http 
 # omc-test-kb-http   LoadBalancer   10.109.208.169   10.109.208.169   5601:30123/TCP   6m51s


#or comande : 
minikube service omc-test-kb-http --url --namespace=elastic-system
# http://192.168.49.2:30123 ==> need to be https

and the uiser : elastic
password : $PASSWORD 
#specifc now : 870P1BbQ5x5401o9zVytSgsv


# PASSWORD=$(kubectl get secret omc-test-es-elastic-user -o go-template='{{.data.elastic | base64decode}}')
# echo     "write this down : $PASSWORD " 





